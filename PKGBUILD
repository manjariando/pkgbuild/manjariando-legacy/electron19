# Maintainer: Dan Johansen <strit@manjaro.org>

pkgname=electron19
pkgver=19.1.9
pkgrel=5.1
_major_ver=${pkgver%%.*}
pkgdesc='Build cross platform desktop apps with web technologies - version 19'
arch=('x86_64' 'aarch64')
url='https://electronjs.org/'
license=('MIT' 'custom')
depends=('c-ares' 'ffmpeg' 'gtk3' 'http-parser' 'libevent' 'libnghttp2'
         'libxslt' 'libxss' 'minizip' 'nss' 're2' 'snappy')
optdepends=('kde-cli-tools: file deletion support (kioclient5)'
            'trash-cli: file deletion support (trash-put)'
            "xdg-utils: open URLs with desktop's default (xdg-email, xdg-open)")
provides=("electron${_major_ver}")
conflicts=("electron${_major_ver}")
replaces=('electron-aarch64-bin')
source=("https://github.com/electron/electron/releases/download/v${pkgver}/electron-v${pkgver}-linux-arm64.zip"
        "electron19.desktop")
md5sums=('3959f7692651d8652d1c10d5ee325f84'
         'aad228cd796dfe4106cb2fdcd92bb325')

package() {
  install -dm755 "${pkgdir}/usr/lib/${pkgname}"
  cp -a "${srcdir}"/* "${pkgdir}/usr/lib/${pkgname}"

  # remove symlinks to build files
  rm -f "${pkgdir}/usr/lib/${pkgname}/electron-v${pkgver}-linux-arm64.zip"
  rm -f "${pkgdir}/usr/lib/${pkgname}/${pkgname}.desktop"

  install -dm755 "${pkgdir}/usr/share/licenses/${pkgname}"
  for l in "${pkgdir}/usr/lib/${pkgname}"/{LICENSE,LICENSES.chromium.html}; do
    ln -s  \
      $(realpath --relative-to="${pkgdir}/usr/share/licenses/${pkgname}" ${l}) \
      "${pkgdir}/usr/share/licenses/${pkgname}"
  done

  install -dm755 "${pkgdir}"/usr/bin
  ln -s ../lib/${pkgname}/electron "${pkgdir}"/usr/bin/${pkgname}

  # Install .desktop and icon file (see default_app-icon.patch)
  install -Dm644 -t "${pkgdir}/usr/share/applications" ${pkgname}.desktop
}
